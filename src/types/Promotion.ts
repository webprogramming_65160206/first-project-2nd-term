import type { PromotionItem } from "./PromotionItem";

type Promotion = {
  id: number;
  code: string;
  name: string;
  startdate: string;
  enddate: string;
  status: 'enable' | 'disable'; // Specify that status can only be 'on' or 'off'
  PromotionItem?: PromotionItem[] | undefined;
};

export { type Promotion };