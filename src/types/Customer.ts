import type { Member } from './Member'

type Customer = {
  id: number
  name: string
  tel: string
  point: number
  accumulate: number
  discount: number
}

export type { Customer, Member }
