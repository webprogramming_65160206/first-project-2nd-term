import type { Product } from "./Product";
import type { Promotion } from "./Promotion";

type PromotionItem = {
  id: number;
  productid: number | null
  product?: Product | null
  conditionQty: number | null
  conditionPrice: number | null
  discountPercent: number | null
  discountPrice: number | null
  status: string | null
  // ถ้าอยากให้เหมือนจารย์โกเมศแล้วเวลาเหลือให้กลับมาทำ
};
export { type PromotionItem };